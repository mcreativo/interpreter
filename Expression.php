<?php


interface Expression
{
    /**
     * @param Vector[] $variables
     * @return mixed
     */
    public function evaluate($variables);
}