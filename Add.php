<?php


class Add implements Expression
{
    /**
     * @var Expression
     */
    protected $expression1;

    /** @var  Expression */
    protected $expression2;

    /**
     * @param Expression $expression1
     * @param Expression $expression2
     */
    function __construct(Expression $expression1, Expression $expression2)
    {
        $this->expression1 = $expression1;
        $this->expression2 = $expression2;
    }


    public function evaluate($variables)
    {
        return $this->expression1->evaluate($variables)->add($this->expression2->evaluate($variables));
    }

} 