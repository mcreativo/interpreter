<?php


class Multiply implements Expression
{

    /**
     * @var Expression
     */
    protected $expression1;

    /**
     * @var Expression
     */
    protected $expression2;

    /**
     * @param Expression $expression1
     * @param Expression $expression2
     */
    function __construct($expression1, $expression2)
    {
        $this->expression1 = $expression1;
        $this->expression2 = $expression2;
    }


    public function evaluate($variables)
    {
        if ($this->expression2 instanceof Constant) {
            return $this->expression1->evaluate($variables)->multiply($this->expression2->evaluate($variables));
        } else {
            return $this->expression2->evaluate($variables)->multiply($this->expression1->evaluate($variables));
        }

    }

} 