<?php


class Tokenizer
{
    /**
     * @param string $input
     * @return array
     */
    public function tokenize($input)
    {
        $tokens = preg_split('/([*+()])\s*|([-]?[\d.]+)\s*/', $input, -1,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $output = [];
        $stack = [];
        foreach ($tokens as $token) {
            switch ($token) {
                case '+':
                    while ($var = end($stack)) {
                        if ($var == '*' or $var == '+') {
                            array_push($output, array_pop($stack));
                        } else {
                            break;
                        }
                    }
                    array_push($stack, $token);
                    break;
                case '*':
                    while ($var = end($stack)) {
                        if ($var == '*') {
                            array_push($output, array_pop($stack));
                        } else {
                            break;
                        }
                    }
                    array_push($stack, $token);
                    break;
                case '(':
                    array_push($stack, $token);
                    break;
                case ')':
                    while ($var = array_pop($stack)) {
                        if ($var !== '(')
                            array_push($output, $var);
                        else
                            break;
                    }
                    break;
                default:
                    array_push($output, $token);
            }
        }
        while ($var = array_pop($stack)) {
            array_push($output, $var);
        }
        return $output;
    }

}