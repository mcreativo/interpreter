<?php


class Constant implements Expression
{

    /**
     * @var int
     */
    protected $integer;


    function __construct($integer)
    {
        $this->integer = $integer;
    }

    public function evaluate($variables)
    {
        return $this->integer;
    }
}