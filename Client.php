<?php




$tokenizer = new Tokenizer();

$tokens = $tokenizer->tokenize('2*(a+b)*-4+c*2+5*a');


$stack = [];
foreach ($tokens as $token) {
    if (is_numeric($token)) {
        array_push($stack, new Constant($token));
    } elseif ($token == '+') {
        array_push($stack, new Add(array_pop($stack), array_pop($stack)));
    } elseif ($token == '*') {
        array_push($stack, new Multiply(array_pop($stack), array_pop($stack)));
    } else {
        array_push($stack, new Variable($token));
    }
}

$expression = array_pop($stack);

$vector = $expression->evaluate([
    'a' => new Vector(2, 3),
    'b' => new Vector(3, 5),
    'c' => new Vector(1, 2)
]);










