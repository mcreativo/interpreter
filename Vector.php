<?php


class Vector
{
    /**
     * @var int x
     */
    protected $x;

    /**
     * @var int y
     */
    protected $y;

    /**
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @param Vector $vector
     * @return Vector
     */
    public function add(Vector $vector)
    {
        return new Vector($this->x + $vector->x, $this->y + $vector->y);
    }

    public function multiply($k)
    {
        return new Vector($this->x * $k, $this->y * $k);
    }
} 