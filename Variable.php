<?php


class Variable implements Expression
{
    /**
     * @var string
     */
    protected $letter;

    /**
     * @param string $letter
     */
    function __construct($letter)
    {
        $this->letter = $letter;
    }

    /**
     * @param Vector[] $variables
     * @return mixed|Vector
     */
    public function evaluate($variables)
    {
        return $variables[$this->letter];
    }
}